import { StatusBar } from "expo-status-bar";
import { NavigationContainer } from "@react-navigation/native";
import Router from "./src/routes";

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar
        barStyle="light-content"
        hidden={false}
        translucent
        backgroundColor="transparent"
      />
      <Router />
    </NavigationContainer>
  );
}
