import apisauce from "apisauce";

const create = () => {
  // const dispatch = useDispatch();

  const api = apisauce.create({
    baseURL: "https://jsonplaceholder.typicode.com/",
    headers: {
      "Cache-Control": "no-cache",
      "Content-Type": "application/json"
    },
    timeout: 30000
  });

  // inisialisasi
  const GET = api.get;
  const POST = api.post;
  const DELETE = api.delete;
  const PUT = api.put;

  // end point
  const GET_LIST = () => GET("posts");
  const GET_BY_ID = (data) => GET(`posts/${data}`);

  return {
    GET_LIST,
    GET_BY_ID
  };
};

export default { create };
