import React, { useEffect, useRef, useState } from "react";
import {
  Animated,
  BackHandler,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { Feather, FontAwesome } from "@expo/vector-icons";
import { API } from "../../services";

const List = ({ navigation }) => {
  const [data, setData] = useState([]);
  const [isVisible, setIsVisible] = useState(true);
  const ref = useRef();

  useFocusEffect(
    React.useCallback(() => {
      // Do something when the screen is focused
      BackHandler.addEventListener("hardwareBackPress", backPressed);
      GetList();

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
        BackHandler.removeEventListener("hardwareBackPress", backPressed);
      };
    }, [])
  );

  const backPressed = () => {
    if (Platform.OS === "ios") {
      // Minimize the app on iOS
      return true;
    } else {
      Alert.alert(
        "Exit App",
        "Exiting the application?",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "OK",
            onPress: () => BackHandler.exitApp()
          }
        ],
        {
          cancelable: false
        }
      );
      return true;
    }
  };

  const GetList = async () => {
    await API.create()
      .GET_LIST()
      .then((res) => {
        if (res.status === 200) {
          setData(res.data);
        }
      });
  };

  const animationController = useRef(new Animated.Value(0)).current;
  const AnimatedPressable = Animated.createAnimatedComponent(TouchableOpacity);

  const scale = animationController.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1]
  });

  useEffect(() => {
    Animated.timing(animationController, {
      duration: 150,
      toValue: isVisible ? 1 : 0,
      useNativeDriver: true
    }).start();
  }, [isVisible]);

  const onFabPress = () => {
    ref.current.scrollToOffset({ animated: true, offset: 0 });
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        showsVerticalScrollIndicator={false}
        ref={ref}
        onScroll={(event) => {
          if (
            event.nativeEvent.contentOffset.y > 0 ||
            event.nativeEvent.contentOffset.y < 0
          ) {
            setIsVisible(true);
          } else {
            setIsVisible(false);
          }
        }}
        scrollEventThrottle={1}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                navigation.navigate("Detail", {
                  itemId: item.id
                })
              }
              style={[
                styles.contentFlatlist,
                {
                  marginTop: index === 0 ? 10 : 0
                }
              ]}
              key={index}
            >
              <Feather
                name="mail"
                size={50}
                color="grey"
                style={{ marginRight: 10 }}
              />
              <View>
                <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                  {item.userId}
                </Text>

                <Text numberOfLines={1} style={{ fontWeight: "bold" }}>
                  {item.title.length < 50
                    ? `${item.title}`
                    : `${item.title.substring(0, 47)}...`}
                </Text>

                <Text numberOfLines={1} style={{ color: 'grey' }}>
                  {item.body.length < 50
                    ? `${item.body}`
                    : `${item.body.substring(0, 47)}...`}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />

      <AnimatedPressable
        style={[styles.fab, { transform: [{ scale }] }]}
        onPress={() => {
          onFabPress();
        }}
      >
        <View style={styles.contentFab}>
          <FontAwesome name="angle-double-up" size={30} color="white" />
        </View>
      </AnimatedPressable>
    </View>
  );
};

export default List;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  fab: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: "#055DB3",
    position: "absolute",
    bottom: 20,
    right: 20,
    justifyContent: "center",
    opacity: 0.9,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8
  },
  contentFab: {
    justifyContent: "center",
    alignItems: "center"
  },
  contentFlatlist: {
    marginHorizontal: 10,
    borderWidth: 0.5,
    borderColor: "grey",
    borderRadius: 10,
    marginBottom: 10,
    padding: 10,
    flexDirection: "row",
    alignItems: "center"
  }
});
