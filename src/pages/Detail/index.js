import React, { useEffect, useState } from "react";
import { BackHandler, StyleSheet, Text, View } from "react-native";
import { Feather, MaterialIcons } from "@expo/vector-icons";

import { API } from "../../services";

const Detail = ({ route, navigation }) => {
  const { itemId } = route.params;

  const [title, setTitle] = useState(null);
  const [body, setBody] = useState(null);
  const [userId, setUserId] = useState(null);

  useEffect(() => {
    console.log(itemId);
    BackHandler.addEventListener("hardwareBackPress", backPressed);
    GetById();

    return () => {
      // Do something when the screen is unfocused
      // Useful for cleanup functions
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
    };
  }, []);

  const GetById = async () => {
    await API.create()
      .GET_BY_ID(itemId)
      .then((res) => {
        if (res.status === 200) {
          console.log(res.data);
          setTitle(res.data.title);
          setBody(res.data.body);
          setUserId(res.data.userId);
        }
      });
  };

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  return (
    <View style={styles.container}>
      <View style={{ padding: 15 }}>
        <Text style={{ fontSize: 20 }}>{title}</Text>
      </View>

      <View style={{ borderWidth: 0.2, borderColor: "grey" }} />

      <View style={{ flexDirection: "row", padding: 15 }}>
        <Feather
          name="mail"
          size={50}
          color="grey"
          style={{ marginRight: 10 }}
        />
        <View>
          <Text style={{ fontWeight: "bold", fontSize: 15 }}>{userId}</Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={{ fontSize: 12, marginRight: 5 }}>to me</Text>
            <MaterialIcons name="keyboard-arrow-down" size={24} color="black" />
          </View>
        </View>
      </View>

      <View style={{ borderWidth: 0.2, borderColor: "grey" }} />

      <View style={{ padding: 15 }}>
        <Text style={{fontSize: 15}}>{body}</Text>
      </View>
    </View>
  );
};

export default Detail;
const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  }
});
